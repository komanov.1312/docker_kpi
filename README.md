nextjs + nginx + php + mysql + phpmyadmin

## Nextjs
localhost:3000
Nextjs запускается в контейнере в дев режиме с поддержкой хот релоуда.

## Вывод главной страницы index.php

Получаем вывод статики php на 80-й порт

## PhpMyadmin

phpMyAdmin на 88-м порту.

## Docker, docker-compose

docker-compose up -d --build
Cоздаём контейнеры для запуска и запускаем их, не блокируя консоль

docker-compose down
Остановить процесс выполнения контейнеров 

docker system prune
Для чистки системы от созданных докером контейнеров, volumes, сетей. Она нужна, если мы закончили работу над проектом и не хотим хранить кучу мусора на компьютере.